<?php $translate['wpml-no'] = mfn_opts_get('translate') ? mfn_opts_get('translate-wpml-no','No translations available for this page') : __('No translations available for this page','betheme'); ?>

<?php if( mfn_opts_get('action-bar')): ?>
	<div id="Action_bar">
		<div class="container">
			<div class="column one">
			
				<ul class="contact_details">
					<?php
						if( $header_slogan = mfn_opts_get( 'header-slogan' ) ){
							echo '<li class="slogan">'. $header_slogan .'</li>';
						}
						if( $header_phone = mfn_opts_get( 'header-phone' ) ){
							echo '<li class="phone"><i class="icon-phone"></i><a href="tel:'. str_replace(' ', '', $header_phone) .'">'. $header_phone .'</a></li>';
						}					
						if( $header_phone_2 = mfn_opts_get( 'header-phone-2' ) ){
							echo '<li class="phone"><i class="icon-phone"></i><a href="tel:'. str_replace(' ', '', $header_phone_2) .'">'. $header_phone_2 .'</a></li>';
						}					
						if( $header_email = mfn_opts_get( 'header-email' ) ){
							echo '<li class="mail"><i class="icon-mail-line"></i><a href="mailto:'. $header_email .'">'. $header_email .'</a></li>';
						}
					?>
				</ul>
				
				<?php 
					if( has_nav_menu( 'social-menu' ) ){
						mfn_wp_social_menu();
					} else {
						get_template_part( 'includes/include', 'social' );						
					}
				?>

			</div>
		</div>
	</div>
<?php endif; ?>

<?php 
	if( mfn_header_style( true ) == 'header-overlay' ){
		
		// Overlay ----------
		echo '<div id="Overlay">
				<div class="title-menu">You are at</div><!-- END title-menu -->
					<div class="subtitle-menu">
						<ul>
							<li class="subtitle-href spring-menu-href active" data-target="">	SPRING PROFESSIONAL	</li>
							<li class="subtitle-href CE-menu-href " data-target="">				CUSTOMER ENGAGEMENT	</li>
							<li class="subtitle-href YP-menu-href " data-target="">				YOUNG PROFESSIONALS	</li>
						</ul>
				 	</div><!-- END subtitle-menu -->
					
					<div class="body-menu">

						<div class="spring-menu-items">
							<nav>
									<ul>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/">
												Home
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/3-9-ce-vacancy-overview-v07-09/">
												Vacatures
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/4-7-yp-companies-v03-10/">
												Voor bedrijven
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/3-14-spring-contact-v-28-09/">
												Contact
											</a>
										</li>
									</ul>
								</nav>
			';
						// mfn_wp_overlay_menu();
			echo '
						</div><!-- END spring-menu-items -->
						<div class="secondary_menu_wrapper">
							<div class="CE-menu-items">
								<nav>
									<ul>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/3-1-ce-home-v07-09-header-2">
												Home
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/3-2-ce-job-functions-v07-09/">
												Job profielen
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/3-9-ce-vacancy-overview-v07-09">
												Vacatures
											</a>
										</li>
										<li>
											<a href="#">
												Voor bedrijven
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/3-12-ce-success-stories-v28-09">
												Succesverhalen
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/3-14-spring-contact-v-28-09/">
												Contact
											</a>
										</li>
									</ul>
								</nav>
				';			
								// mfn_wp_secondary_menu();
			echo '			</div><!-- END CE-menu-items -->
							<div class="YP-menu-items">
								


								<nav>
									<ul>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/4-1-yp-home-v03-10/">
												Home
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/4-2-yp-job-functions-v03-10/">
												Job profielen
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/4-7-yp-companies-v03-10/">
												Voor bedrijven
											</a>
										</li>
										<li>
											<a href="#">
												Work learning program
											</a>
										</li>
										<li>
											<a href="http://spring-candidates.cpi-development.eu/3-14-spring-contact-v-28-09/">
												Contact
											</a>
										</li>
									</ul>
								</nav>




							</div><!-- END YP-menu-items -->
						</div><!-- END secondary_menu_wrapper -->

					</div><!-- END body-menu -->

					<div class="footer-menu">
				  		<div class="footer-menu--left address_side">
							<p>
								 <a href="mailto:hello@springprofessional.be">hello@springprofessional.be</a>
							</p>
						</div><!-- END footer-menu--left address_side -->

						<div class="footer-menu--right social_side">
				';

								if( has_nav_menu( 'social-menu' ) ) {
									echo '<span> follow us </span>';
									mfn_wp_social_menu();
								}
								else {
									get_template_part( 'includes/include', 'social' );						
								}

			echo '		</div><!-- END footer-menu--right social_side -->
					</div><!-- END footer-menu -->
				</div><!-- END #Overlay -->
			';
		
		
			do_action('icl_language_selector');


		// Button ----------
		echo '<a class="overlay-menu-toggle" href="#">';
			echo '	<span class="hamburger_own">
						<span></span>
						<span></span>
						<span></span>
					</span>
				';
			// echo '<i class="open icon-menu-fine"></i>';
			// echo '<i class="close icon-cancel-fine"></i>';
		echo '</a>';
	}
?>



<script type="text/javascript">
	var speed = 100 ;
$(document).ready(function(){
	$(".spring-menu-items").slideDown( speed );
	$(".CE-menu-items").slideUp( speed );
	$(".YP-menu-items").slideUp( speed );
});

$(".subtitle-href.spring-menu-href").on("click", function(){
	$(".spring-menu-items").slideDown( speed );
	$(".CE-menu-items").slideUp( speed );
	$(".YP-menu-items").slideUp( speed );
});
$(".subtitle-href.CE-menu-href").on("click", function(){
	$(".CE-menu-items").slideDown( speed );
	$(".spring-menu-items").slideUp( speed );
	$(".YP-menu-items").slideUp( speed );
});
$(".subtitle-href.YP-menu-href").on("click", function(){
	$(".YP-menu-items").slideDown( speed );
	$(".spring-menu-items").slideUp( speed );
	$(".CE-menu-items").slideUp( speed );
});

$(".subtitle-href").on("click", function(){
	$(this).addClass("active");
	$(".subtitle-href").not( this ).removeClass("active");
});

// $(document).ready(function(){
// 	if( "#Overlay li" ).hasClass("current-menu-item") {
// 		$(this).closest("div[class$='menu-items']").addClass("THISSSSSSSSSSSSSSSS")
// 	}
// });
</script>




<!-- .header_placeholder 4sticky  -->
<div class="header_placeholder"></div>

<div id="Top_bar" class="loading">

	<div class="container">
		<div class="column one">
		
			<div class="top_bar_left clearfix">
			
				<!-- Logo -->
				<?php get_template_part( 'includes/include', 'logo' ); ?>
			
				<div class="menu_wrapper">
					<?php 
						if( ( mfn_header_style( true ) != 'header-overlay' ) && ( mfn_opts_get( 'menu-style' ) != 'hide' ) ){
	
							// TODO: modify the mfn_header_style() function to be able to find the text 'header-split' in headers array
							
							// #menu --------------------------
							if( in_array( mfn_header_style(), array( 'header-split', 'header-split header-semi', 'header-below header-split' ) ) ){
								// split -------
								mfn_wp_split_menu();
							} else {
								// default -----
								mfn_wp_nav_menu();
							}
						
							// responsive menu button ---------
							$mb_class = '';
							if( mfn_opts_get('header-menu-mobile-sticky') ) $mb_class .= ' is-sticky';

							echo '<a class="responsive-menu-toggle '. $mb_class .'" href="#">';
								if( $menu_text = trim( mfn_opts_get( 'header-menu-text' ) ) ){
									echo '<span>'. $menu_text .'</span>';
								} else {
									echo '<i class="icon-menu-fine"></i>';
								}  
							echo '</a>';
							
						}
					?>					
				</div>			
<!--
				<div class="secondary_menu_wrapper">
					<!-- #secondary-menu - ->
					<?php // mfn_wp_secondary_menu(); ?>
				</div>
-->
				<div class="banner_wrapper">
					<?php mfn_opts_show( 'header-banner' ); ?>
				</div>
				
				<div class="search_wrapper">
					<!-- #searchform -->
					
					<?php get_search_form( true ); ?>
					
				</div>				
				
			</div>
			
			<?php 
				if( ! mfn_opts_get( 'top-bar-right-hide' ) ){
					get_template_part( 'includes/header', 'top-bar-right' );
				}
			?>
			
		</div>
	</div>
</div>