<?php  get_header(); check_vacancy_id() ?>

<?php global $wp_query; if ($wp_query->have_posts()) : $wp_query->the_post(); ?>

				<section id="section__1--slider" class="section__1--slider">
					<div class="row">
						<div class="container">

							<div class="section__1--main_title_block">
								<div class="main_title">

									<h1 class="title">
										Teamleader Customer
										<br>
										Service
									</h1>

									<h5 class="sub_title">
										Company name in Machelen
									</h5>

									<a class="btn btn-rounded btn-pink btn-medium" title="" href="#applyform">
										Apply now
									</a>

								</div>
							</div>

						</div><!-- END container -->
					</div><!-- END row -->
				</section><!-- END section__1 -->



				<div id="row_scroll" class="row_scroll top-200 overlap">
					<div class="row">
						<div class="container">
							<div class="col-xs-12">
								<button class=" scroll_btn_next sd_white rotate90" type="button" data-target="#custom-section--top_tab">
									SCROLL
								</button>

								<div class="vr-line vr-white vr-bottom"></div>

							</div><!-- END col-xs- -->
						</div><!-- END container -->
					</div><!-- END row -->
				</div><!-- END row_scroll -->






				<section id="custom-section--top_tab" class="custom-section--top_tab">
					<div class="row">
						<div class="container">





							<div id="social_row_block" class="social_row_block row">
								<div class="social_block">
									<a class="" href="" title="">
										<span class="fa fa-facebook"></span>
									</a>
									<a class="" href="" title="">
										<span class="fa fa-twitter"></span>
									</a>
									<a class="" href="" title="">
										<span class="fa fa-linkedin"></span>
									</a>
								</div>
							</div>




							<div id="top_tab" class="top_tab">
								<div class="img_row">
									<img src="http://spring-candidates.cpi-development.eu/wp-content/uploads/2017/08/CE-main-customer_service_ninja-w.svg" alt="img" width="125" height="125">
								</div>

								<p>CUSTOMER SERVICE</p>

								<h2><?php the_title(); ?></h2>
							</div><!-- END top_tab -->

							<div id="info_block" class="info_block">
								<div class="row">
									<div class="col-xs-12">

										<div class="left-side">
											<div class="info_item">
												<span class="img_box">
													<img src="http://spring-candidates.cpi-development.eu/wp-content/uploads/2017/08/icon-location-white.svg" alt="img" width="35" height="auto">
												</span>
												<span class="city">
													<?= get_post_meta($post->ID, 'province', true); ?>
												</span>
												<span class="label">
													LOCATION
												</span>
											</div><!-- END info_item -->

											<div class="info_item">
												<span class="img_box">
													<img src="http://spring-candidates.cpi-development.eu/wp-content/uploads/2017/08/icon-bag-white.svg" alt="img" width="35" height="auto">
												</span>
												<span class="contact_duration">
													UNSPECIFIED
												</span>
												<span class="label">
													CONTRACT DURATION
												</span>
											</div><!-- END info_item -->
										</div><!-- END left-side -->

										<div class="right-side">
											<div class="info_item">
												<span class="img_box">
													<img src="http://spring-candidates.cpi-development.eu/wp-content/uploads/2017/08/icon-calendar-white.svg" alt="img" width="35" height="auto">
												</span>
												<span class="date">
													<?= get_post_meta($post->ID, 'VacancyPostingStartDate', true); ?>
												</span>
												<span class="label">
													PUBLISHED
												</span>
											</div><!-- END info_item -->

											<div class="info_item">
												<span class="img_box">
													<img src="http://spring-candidates.cpi-development.eu/wp-content/uploads/2017/08/icon-hashtag-white.svg" alt="img" width="35" height="auto">
												</span>
												<span class="category">
													COMPANY CULTURE
												</span>
												<span class="label">
													<?php $tax = wp_get_post_terms($post->ID, 'function_groups'); echo $tax[0]->name; ?>
												</span>
											</div><!-- END info_item -->
										</div><!-- END right-side -->
									</div><!-- END col-xs- -->

								</div><!-- END row -->
							</div><!-- END #info_block -->

						</div><!-- END container -->
					</div><!-- END row -->
				</section><!-- END custom-section--top_tab -->


				<section id="section--row_scroll_down" class="section--row_scroll_down after_top_tab_blue_block">
					<div class="container">
						<div id="row_scroll" class="row_scroll">
							<button class="scroll_btn_next rotate90 sd_blue" type="button" data-target="#custom-section--vacancies">
								SCROLL
							</button>

							<div class="vr-line vr-blue vr-bottom"></div>
						</div><!-- END row_scroll -->
					</div><!-- END container -->
				</section><!-- END section--row_scroll_down -->


				<section id="custom-section--vacancies">
					<div class="row">
						<div class="container">
							<div class="col-xs-12 col-sm-12 col-md-3">

								<div id="own_sidebar" class=" own_sidebar ">
									<div class="sidebar_title---block">
										<div class="sidebar_title">
											<p>
												CATEGORIES
											</p>
											<h4>
												Job profiles
												Customer
												Engagement.
											</h4>
										</div>
									</div><!-- END sidebar_title---block -->

									<div class="">
										<div class="part_of_sidebar">
											<button class="">
												<span>
													ALL FUNCTIONS
												</span>
												<i class="fa fa-caret-down fa-2x"></i>
											</button>

											<ul>
												<li class="sidebar-item">
													<a class="customer_service_ninja" href="#" title="">
														<span>
															CUSTOMER SERVICE
														</span>
														<!-- <img src="/wp-content/uploads/2017/09/CE-main-customer_service_ninja-gy.svg" alt="img" width="70" height="auto" /> -->
													</a>
												</li>

												<li class="sidebar-item">
													<a class="digital_marketing_hero" href="#" title="">
														<span>
															MARKETING & COMMUNICATION
														</span>
														<!-- <img src="/wp-content/uploads/2017/09/CE-main-digital_marketing_hero-gy.svg" alt="img" width="70" height="auto" /> -->
													</a>
												</li>

												<li class="sidebar-item">
													<a class="sales_gladiator" href="#" title="">
														<span>
															SALES
														</span>
														<!-- <img src="/wp-content/uploads/2017/09/CE-main-sales_gladiator-gy.svg" alt="img" width="70" height="auto" /> -->
													</a>
												</li>

												<li class="sidebar-item">
													<a class="product_captain" href="#" title="">
														<span>
															PRODUCT MANAGEMENT
														</span>
														<!-- <img src="/wp-content/uploads/2017/09/CE-main-product_captain-gy.svg" alt="img" width="70" height="auto" /> -->
													</a>
												</li>

												<li class="sidebar-item">
													<a class="credit_collection_sheriff" href="#" title="">
														<span>
															FINANCE
														</span>
														<!-- <img src="/wp-content/uploads/2017/09/CE-main-credit_collection_sheriff-gy.svg" alt="img" width="70" height="auto" /> -->
													</a>
												</li>

												<li class="sidebar-item">
													<a class="joker_profiles" href="#" title="">
														<span>
															OTHER
														</span>
														<!-- <img src="/wp-content/uploads/2017/09/CE-main-joker_profiles-gy.svg" alt="img" width="70" height="auto" /> -->
													</a>
												</li>
											</ul>
										</div><!-- END part_of_sidebar -->
									</div>
								</div><!-- END own_sidebar -->
							</div><!-- END col- -->

							<div class="col-xs-12 col-sm-12	col-md-9 job_description">

								<div id="job_description">
									<div class="row">
										<div class="user_actions">
											<button class="" type="button">
												<img src="http://spring-candidates.cpi-development.eu/wp-content/uploads/2017/08/icon-mail-pink.svg" alt="img" width="35" height="auto">
											</button>
											<button class="" type="button">
												<img src="http://spring-candidates.cpi-development.eu/wp-content/uploads/2017/08/star-pink.svg" alt="img" width="35" height="auto">
											</button>
											<button class="" type="button">
												<img src="http://spring-candidates.cpi-development.eu/wp-content/uploads/2017/08/icon-print-pink.svg" alt="img" width="35" height="auto">
											</button>
										</div>
									</div><!-- END row -->

									<div class="row">
										<h2>
											Job description.
										</h2>

										<?php the_content(); ?>
									</div><!-- END row -->

								</div><!-- END job_description -->

							</div><!-- END col- -->

						</div><!-- END container -->
					</div><!-- END row -->
				</section><!-- END custom-section--vacancies -->




				<section id="custom-section--apply_by_filling" class="custom-section--apply_by_filling">
					<div id="custom-section--apply_by_filling--row" class="row">
						<div class="container">

                            <a name='applyform'></a>
							<div class="row">
								<div class="col-xs-12 apply_by_filling--title">
									<p class="text-uppercase">
										FILL THE FORM
									</p>

									<h2>
										Apply by filling the form below
									</h2>

									<p>
										Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam excepteur sint obcaec.
									</p>
<!--
									<div class="btn_row">
										<a class="btn  btn-pink btn-outlined btn-invert btn-rounded btn-medium" href="#">
											Sign up with linkedin
										</a>
									</div>-->
								</div>
							</div>

                            <!--
							<div class="row">
								<form class="apply_by_filling--form">
									<p class="names_row">
										<input class="first_name inp inp-rounded inp-outlined inp-blue inp-medium" type="text" placeholder="First Name">
										<input class="last_name inp inp-rounded inp-outlined inp-blue inp-medium" type="text" placeholder="Last Name">
									</p>

									<p>
										<input class="inp inp-rounded inp-outlined inp-blue inp-medium" type="email" placeholder="Email">
									</p>

									<p>
										<input class="inp inp-rounded inp-outlined inp-blue inp-medium" type="text" placeholder="Telephone number">
									</p>

									<p>
										<input class="inp inp-rounded inp-outlined inp-blue inp-medium" type="text" placeholder="Zip code">
									</p>

									<p class="upload_row">
										<input class="inp inp-rounded inp-outlined inp-blue inp-medium" type="text" placeholder="CV/Video,Cover letter....">

										<label class="upload_file">Upload file
											<input class="" type="file">
										</label>
									</p>

									<p>
										<input id="terms_and_conditions" type="checkbox">
										<label class="checkmark" for="terms_and_conditions">Terms and conditions</label>
									</p>

									<div class="btn_row">
										<button class="btn btn-pink btn-rounded btn-medium" type="submit">
											Submit appilcation
										</button>
									</div>
								</form>
							</div>
							-->

							<div class="row">
										<?php 
					echo do_shortcode('[jobapplication pubid="'. get_post_meta(get_the_ID(), 'vacancy_id', true) .'"]');
				?>

							</div>

						</div><!-- END container -->
					</div><!-- END row #custom-section--apply_by_filling--row -->
				</section><!-- END custom-section--apply_by_filling -->










				<section id="custom-section--not_what_youre_looking_for" class="custom-section--not_what_youre_looking_for">
					<div class="container">
						<div class="row">

							<div id="custom-section--not_what_youre_looking_for--blue_block" class="custom-section--not_what_youre_looking_for--blue_block">
								<p>
									CUSTOMER ENGAGEMENT
								</p>
								<h2>
									Not what you’re looking for?
								</h2>

								<p class="width-80">
									Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrum exercitationem ullam corporis suscipit laboriosam. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia&nbsp;deserunt mollit anim id est laborum.
								</p>

								<div class="btn_row">
									<a class="btn btn-blue btn-outlined btn-rounded btn-medium" href="#" title="">
										Introduce yourself
									</a>
									<a class="btn btn-blue btn-outlined btn-rounded btn-medium" href="#" title="">
										More vacancies
									</a>
								</div><!-- END btn_row -->
							</div><!-- END custom-section--not_what_youre_looking_for--blue_block -->

						</div><!-- END row -->
					</div><!-- END container -->
				</section><!-- END custom-section--not_what_youre_looking_for -->




				<section id="CE_vacancy_detail--page--slide-we_are_hiring" class="CE_vacancy_detail--page--slide-we_are_hiring">
					<div class="container">
						<div class="row">

							<!-- in this section only bgi -->

						</div><!-- END row -->
					</div><!-- END container -->
				</section><!-- END CE_vacancy_detail--page -e-we_are_hiring -->




				<section id="section--row_scroll_top" class="section--row_scroll_top">
					<div id="row_scroll" class="row_scroll top-100">
						<button class="scroll_btn_next sd_blue rotate270" type="button" data-target="#custom-section--top_tab">
							TOP
						</button>

						<div class="vr-line vr-blue vr-top"></div>
					</div>
				</section>




				<section id="custome-section--you_vacancies_on_this_page" class="custome-section--you_vacancies_on_this_page">
					<div class="container">
						<div class="row">

							<p>
								YOUR VACANCIES ON THIS PAGE?
							</p>

							<h2>
								Introduce yourse if and get notified when we have interesting vacancies for you.
							</h2>

							<p>
								Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
								<br>
								Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.
							</p>

							<div class="btn_row">
								<a class="btn btn-rounded btn-pink btn-small" href="#">
									Introduce yourself
								</a>
							</div><!-- END btn_row -->

						</div><!-- END row -->
					</div><!-- END container -->
				</section>

	


				<script type="text/javascript">
					jQuery(function($){
						$("#own_sidebar button").on("click", function(){
							if ( $(this).parent(".part_of_sidebar").find("ul li").is(":hidden") ){
								$(this).parent(".part_of_sidebar").find("ul li").slideDown("slow");
											// $(this).parent(".part_of_sidebar").find("ul li").show("slow");
											console.log("open");
										}
										else {
											$(this).parent(".part_of_sidebar").find("ul li").slideUp("slow");
											// $(this).parent(".part_of_sidebar").find("ul li").hide("slow");
											console.log("close");
										}
									});

						$('body').addClass('CE_vacancy_detail--page');

						$('button.scroll_btn').on('click', function(){
							$('body, html').animate({
								scrollTop: $(this).closest('section').offset().top - $(this).closest('section').height() - 120
							}, 400);
						});

						$('button.scroll_btn_next').on('click', function(){
							var data_target = $(this).attr("data-target");
							var target = $(data_target).offset().top;
							$('body, html').animate({
								scrollTop: target - 120
							}, 400);
						});
					}); // jQuery(function($)
				</script>

<?php endif; ?>
<?php get_footer(); ?>